Description:
------------

The Dockerfile for https://hub.docker.com/r/garethcmurphy/pytroll/ does not seem to be available, but this Dockerfile was found inside the container.

Original README text:
---------------------

Docker file for pytroll.
This is a self-contained version of the pytroll framework reading, interpretation, and writing of weather satellite data.

See https://www.pytroll.org for more details.

The Dockerfile installs git python-numpy python-scipy python-matplotlib ipython python-pandas python-sympy python-nose python-pip zlib1g libfreetype6-dev wget zip via apt.

Installs mpop mipp pyresample pycoast pyorbital posttroll pykdtree python-geotiepoints trollsift pyspecytral pydecrotate aggdraw gshhg-shp from git.

xRITDecompress from source.

