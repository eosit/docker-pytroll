FROM ubuntu:14.04 
USER root
RUN apt-get update && apt-get -y install sudo  eog 
RUN useradd -ms /bin/bash gmurphy && echo "gmurphy:gmurphy" | chpasswd && adduser gmurphy sudo

ENV gpath /home/gmurphy

RUN mkdir ${gpath}/HRIT
WORKDIR ${gpath}/HRIT/
ADD .   ${gpath}/HRIT/
RUN mkdir ${gpath}/ytroll
RUN mkdir ${gpath}/ytroll/CONFIG
ADD CONFIG/   ${gpath}/ytroll/CONFIG/
RUN cp -r ${gpath}/HRIT/CONFIG ${gpath}/ytroll/
RUN chown -R gmurphy ${gpath}

USER gmurphy
RUN rm -rf ${gpath}/ytroll/*

RUN echo "export PPP_CONFIG_DIR=~/ytroll/CONFIG/" >> .bashrc
USER root
RUN  apt-get update && apt-get install -y git  python-numpy   python-scipy  python-matplotlib   ipython  python-pandas  python-sympy  python-nose  python-pip zlib1g  libfreetype6-dev  wget zip
RUN   apt-get install -y python-pil
RUN   apt-get install -y python-pillow




USER gmurphy
WORKDIR ${gpath}/ytroll
# RUN rm -rf mpop mipp pyresample pycoast pyorbital posttroll pykdtree python-geotiepoints
RUN git clone https://github.com/pytroll/mpop.git
WORKDIR ${gpath}/ytroll/mpop
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll
RUN git clone https://github.com/pytroll/mipp.git
WORKDIR ${gpath}/ytroll/mipp
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/pytroll/pyresample.git
WORKDIR ${gpath}/ytroll/pyresample
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll
RUN git clone https://github.com/pytroll/pycoast
WORKDIR ${gpath}/ytroll/pycoast
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/pytroll/pyorbital.git
WORKDIR ${gpath}/ytroll/pyorbital
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/pytroll/posttroll.git
WORKDIR ${gpath}/ytroll/posttroll
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/storpipfugl/pykdtree.git
WORKDIR ${gpath}/ytroll/pykdtree
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/adybbroe/python-geotiepoints.git
WORKDIR ${gpath}/ytroll/python-geotiepoints
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/pytroll/trollimage.git
WORKDIR ${gpath}/ytroll/trollimage
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/pnuu/trollsift.git
WORKDIR ${gpath}/ytroll/trollsift
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/adybbroe/pyspectral.git
WORKDIR ${gpath}/ytroll/pyspectral
RUN python setup.py install --user 
WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/pytroll/pydecorate.git
WORKDIR ${gpath}/ytroll/pydecorate
RUN cp  ${gpath}/HRIT/setup.py  ${gpath}/ytroll/pydecorate
RUN python setup.py install --user 

RUN cp -r ${gpath}/HRIT/colo*py ${gpath}/.local/lib/python2.7/site-packages/trollimage-v1.0.0-py2.7.egg/trollimage/

WORKDIR ${gpath}/ytroll/
RUN git clone https://github.com/jakul/aggdraw.git
WORKDIR ${gpath}/ytroll/aggdraw
RUN python setup.py build_ext -i
RUN python selftest.py
RUN  python setup.py install --user

WORKDIR ${gpath}/ytroll/
RUN wget http://www.soest.hawaii.edu/pwessel/gshhg/gshhg-shp-2.3.4.zip
RUN unzip gshhg-shp-2.3.4.zip -d ~/ytroll/GSHHS_DATA_ROOT
RUN rm gshhg-shp-2.3.4.zip

# SOFTWARE TO DECOMPRESS xRIT FILES
WORKDIR ${gpath}/ytroll/
RUN wget http://oiswww.eumetsat.org/wavelet/html/1442498693/PublicDecompWT.zip
RUN unzip PublicDecompWT.zip -d ~/root
WORKDIR ${gpath}/root/2.06/xRITDecompress/
RUN make
USER root
RUN cp xRITDecompress /usr/bin


USER gmurphy

RUN export PPP_CONFIG_DIR=${gpath}/ytroll/CONFIG

WORKDIR ${gpath}/HRIT
